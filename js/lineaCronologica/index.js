var LineaCronologicaView = {
    eventos:{},
}

LineaCronologicaView.show = function(data){
    var main = document.getElementById("main");
    main.innerHTML = "";
    LineaCronologicaView.eventos = data;
    main.appendChild(LineaCronologicaView.getTemplate());
}

LineaCronologicaView.getTemplate = function(){
    var template = document.createElement("div");
    template.id = "lineaCronologica";
    template.className = "columns fullscreen";
    LineaCronologicaView.addCenturies(template);
    return template;
}

LineaCronologicaView.addCenturies = function(template){
    for (var i in LineaCronologicaView.eventos) {
        var century = document.createElement("div");
        var title = document.createElement("h1");
        var yearEvents = document.createElement("div");
        yearEvents.className = "anioEventos fullscreen scrollbar";
        yearEvents.siglo = i;
        title.innerHTML = i.toUpperCase();
        century.className = "siglo column has-text-centered";
        century.appendChild(title);
        century.appendChild(yearEvents);
        LineaCronologicaView.addYears(yearEvents,LineaCronologicaView.eventos[i]);
        template.appendChild(century);
    }
}

LineaCronologicaView.addYears = function(template,data){
    for (var i in data) {
        var year = document.createElement("div");
        year.className = "anioEvento";
        year.siglo = template.siglo;
        year.anio = i.replace("_","");
        LineaCronologicaView.addEvent(year,i,data[i]);
        template.appendChild(year);
    }
}

LineaCronologicaView.addEvent = function(template,value){
    if(!value.length) return;
    var eventData = document.createElement("a");
    var title = document.createElement("h2");
    title.innerHTML = template.anio;
    title.className = "is-size-4";
    eventData.href = "./?siglo="+template.siglo+"&anio="+template.anio;
    eventData.className = "anio";
    eventData.appendChild(title);
    /*if(value[0].imagenes){
        var image = document.createElement("img");
        image.src = value[0].imagenes[0].src;
        eventData.appendChild(image);
    }*/
    template.appendChild(eventData);
}