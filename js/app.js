function getParams() {
    var prmstr = window.location.search.substr(1);
    return prmstr != null && prmstr != "" ? paramsToArray(prmstr) : {};
}

function paramsToArray(prmstr) {
    var params = {};
    var prmarr = prmstr.split("&");
    for (var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

function loadComponent(args){
    if(Object.keys(args).length){
        return EventoView.show(eventos,args); 
    }
    return LineaCronologicaView.show(eventos);
}

var params = getParams();
loadComponent(params);