var EventoView = {
    eventos: []
}

EventoView.show = function(data,args){
    var main = document.getElementById("main");
    main.innerHTML = "";
    if(args.anio) args.anio = "_"+args.anio;
    if(!args.siglo || !args.anio || !data[args.siglo] || !data[args.siglo][args.anio]){
        window.location.href = "./";
        return
    }
    document.title = "Año "+args.anio;
    EventoView.eventos = data[args.siglo][args.anio];
    main.appendChild(EventoView.getTemplate());
}

EventoView.getTemplate = function(){
    var template = document.createElement("div");
    template.className = "anioHistorico";
    template.innerHTML = JSON.stringify(EventoView.eventos);
    return template;
}