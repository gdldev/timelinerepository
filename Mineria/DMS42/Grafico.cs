﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization;

namespace DMS42
{
    public partial class Grafico : Form
    {
        public Grafico()
        {
            InitializeComponent();
        }

        public Grafico(List<string> llaves, List<int> valores)
        {
            InitializeComponent();
            chart1.Series["s2"].Points.DataBindXY(llaves, valores);
            chart1.Visible = true;
        }
        private void btn_Aceptar_Grafico_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
