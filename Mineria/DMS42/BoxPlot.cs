﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DMS42
{
    public partial class BoxPlot : Form
    {
        public BoxPlot()
        {
            InitializeComponent();
        }
        public BoxPlot(List<double> elements)
        {
            InitializeComponent();
            chart1.Series["s1"].Points.DataBindY(elements);
            chart1.Series["s1"].Enabled = false;
            chart1.Series["BoxPlotSeries"]["BoxPlotSeries"] = "s1";
        }

        private void btn_Aceptar_boxplot_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
