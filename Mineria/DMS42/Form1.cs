﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace DMS42
{

    public partial class Form_Main : Form
    {
        Datos datos = new Datos(); //Objeto que hace casi todo
        public int filas = 0;
        public bool primerejecucion = false;
        public Form_Main()
        {
            InitializeComponent();
        }
        private void Form_Main_Load(object sender, EventArgs e)
        {
            infoContent.Text = "";
        }

        private void cargarArchivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Datos datos = new Datos();
            datos.CargarArchivo();
            datos.LeerArchivo();
            infoContent.Text = datos.infoGeneral;
            datos.CrearColumnas();
            dataGrid.DataSource = datos.tabla;
            dataGrid.AllowUserToAddRows = false;
            dataGrid.RowHeadersVisible = false;
            datos.CrearFilas();
            datos.EvaluarExpresiones(dataGrid);
            datos.cambios = false;
        }

        private void dataGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //Volver a evaluar expresiones cuando cambian el valor de una celda
            datos.EvaluarExpresiones(dataGrid);
            //Indicar que se han realizado cambios
            datos.cambios = true;
        }

        private void dataGrid_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {   
            //Evitar que se pueda reordenar con la cabecera
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void dataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //DETERMINAR SI SE PUEDE REALIZAR ANALISIS UNIVARIABLE
            Color bad = new Color();
            Color good = new Color();
            bad = Color.DarkOrange;
            good = Color.White;
            bool sePuedeAnalizar=true;
            string tipoAnalisis = "";
            string[] aux = datos.atributos.Split(',');
            List<string> lista = new List<string>();
            if (e.ColumnIndex != 0)
            {
                for (int i = 0; i < datos.numeroInstancias-1; i++)
                {
                    lista.Add(dataGrid.Rows[i].Cells[e.ColumnIndex].Value.ToString());
                    if (dataGrid.Rows[i].Cells[e.ColumnIndex].Style.BackColor == bad)
                    {
                        sePuedeAnalizar = false;
                    }
                }
                if (sePuedeAnalizar)
                {
                    tipoAnalisis = datos.tiposAtributos[aux[e.ColumnIndex - 1]];
                    if(tipoAnalisis == "numeric")
                    {
                        datos.AnalisisNumerico(lista);
                    }
                    else
                    {
                        datos.AnalisisCategorico(lista);
                    }
                }
                else
                {
                    MessageBox.Show("No se puede realizar un analisis debido a que una o mas instancias\nno cumplen con las restricciones del atributo");
                }
            }
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!datos.cambios)
            {
                MessageBox.Show("No se han realizado cambios desde la ultima vez que se guardo.");
            }
            else {
                datos.GuardarCambios(dataGrid);
                datos.cambios = false;
            }
        }

        private void btn_nuevaInstancia_Click(object sender, EventArgs e)
        {//Agregar nueva instancia
            string[] auxAttribs = datos.atributos.Split(',');
            DataRow row = datos.tabla.NewRow();
            row["No."] = datos.numeroInstancias.ToString();
            for (int j = 0; j < datos.cantidadAtributos; j++)
            {
                row[auxAttribs[j]] = datos.datoFaltante;
            }
            datos.tabla.Rows.Add(row);
            datos.cambios = true;
            datos.numeroInstancias++;
            datos.EvaluarExpresiones(dataGrid);
        }

        private void guardarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Permite guardar el archivo con otro nombre
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Guardar guardar = new Guardar();
            guardar.ShowDialog();
            if (guardar.decision == true)
            {
                datos.GuardarCambios(dataGrid);
            }
            this.Close();
        }

        private void guardarComoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|arff files (*.arff)|*.arff|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    //MessageBox.Show(saveFileDialog1.FileName);
                    myStream.Close();
                    datos.GuardarComo(saveFileDialog1.FileName,dataGrid);
                }
            }
        }
    }

    public class Datos
    {
        public string infoGeneral;
        public string nombreArchivo;
        public string nombreConjunto;
        public string datoFaltante;
        public string atributos;
        public int numeroInstancias;
        public int cantidadAtributos;
        public bool cambios;
        public Dictionary<string, string> tiposAtributos;
        public Dictionary<string, string> expRegular;
        public Dictionary<string, string> instancias;
        public DataTable tabla;
        public Datos()
        {
            numeroInstancias = 0;
            cantidadAtributos = 0;
            infoGeneral = "";
            nombreArchivo = "";
            nombreConjunto = "";
            datoFaltante = "";
            atributos = "";
            tiposAtributos = new Dictionary<string, string>();
            expRegular = new Dictionary<string, string>();
            instancias = new Dictionary<string, string>();
            tabla = new DataTable();
            cambios = false;
        }
        public void CargarArchivo() //Abrir el explorador de Windows
        {
            Stream cargar = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = ":c\\";
            openFileDialog1.Filter = "txt files (*.txt)|*txt|arff files(*.arff)|*.arff";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((cargar = openFileDialog1.OpenFile()) != null)
                    {
                        using (cargar)
                        {
                            int rutaTam = openFileDialog1.FileName.Length;
                            string extension = openFileDialog1.FileName.ToString().Substring((rutaTam - 3), 3);
                            if (extension == "txt" || extension == "rff" || extension == "tab") //Asegurar que sea un txt o arff
                            {
                                nombreArchivo = openFileDialog1.FileName.ToString();
                                MessageBox.Show(nombreArchivo + " cargado exitosamente");
                            }
                            else
                            {
                                MessageBox.Show("El archivo cargado no es un archivo de texto, por favor cargue un archivo de texto (.txt|.arff)");
                                CargarArchivo();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: no se puede leer el archivo. Error original: " + ex.Message);
                }
            }
        }
        public void LeerArchivo()
        {
            StreamReader lector = new StreamReader(nombreArchivo);
            char primer;
            string aux;
            do
            {   
                //Leer primer caracter de la linea
                primer = (char)lector.Read();

                if(primer == '%') //Capturar comentarios del archivo
                {
                    aux = lector.ReadLine();
                    for(int i=0; i<aux.Length; i++)
                    {
                        if (aux[i] != '%') infoGeneral = infoGeneral + aux[i];
                    }
                    infoGeneral = infoGeneral + '\n';
                }
                else if(primer == '@')
                {
                    aux = lector.ReadLine();
                    string[] linea = aux.Split(' '); //separar palabras
                    
                    if(linea[0] == "relation")
                    {   //Asignar nombre al conjunto de datos
                        nombreConjunto = linea[1];
                    }
                    else if(linea[0] == "attribute") //
                    {
                        cantidadAtributos++;
                        atributos = atributos + linea[1] + ",";
                        tiposAtributos.Add(linea[1], linea[2]);
                        if (linea.Count() > 4)
                        {
                            for (int i = 4; i < linea.Count(); i++)
                            {
                                linea[3] = linea[3] + " " + linea[i];
                            }
                        }
                        expRegular.Add(linea[1], linea[3]);
                        //MessageBox.Show("Nombre atributo: " + linea[1] + " Tipo: " + linea[2] + " regexp: " + linea[3]);
                    }
                    else if(linea[0] == "missingValue")
                    {   //asignar simbolo para valor faltante
                        datoFaltante = linea[1];
                    }
                    else if(linea[0] == "data")
                    {
                        int cont = 1;
                        do
                        {
                            aux = lector.ReadLine();
                            instancias.Add(cont.ToString(), aux);
                            //MessageBox.Show("Instancia: " + cont + " Valores: " + aux);
                            cont++;
                        } while (!lector.EndOfStream);
                        numeroInstancias = cont;
                        infoGeneral = "Nombre: " + nombreConjunto.ToString() + "\nAtributos: " + cantidadAtributos.ToString() + "\nInstancias: " + (numeroInstancias-1).ToString();
                    }
                }
            } while (!lector.EndOfStream);
            lector.Close();
        }
        public void CrearColumnas()
        {
            tabla.Columns.Add("No."); //Es la columna de enumera cada instancia
            string[] aux = atributos.Split(','); //dividir la cadena que almacena el nombre de los atributos
            for(int i=0;i<aux.Count()-1; i++)
            {
                tabla.Columns.Add(aux[i]); //Crea la columna de cada atributo
            }
        }
        
        public void CrearFilas()
        {
            string[] auxAttribs = atributos.Split(',');
            string[] auxInstancia;
            for(int i=1; i<numeroInstancias; i++)
            {
                auxInstancia = instancias[i.ToString()].Split(',');
                DataRow row = tabla.NewRow();
                row["No."] = i.ToString();
                for(int j=0; j<cantidadAtributos; j++)
                {
                    row[auxAttribs[j]] = auxInstancia[j];
                }
                tabla.Rows.Add(row);
            }
        }

        public void EvaluarExpresiones(DataGridView dataGrid)
        {//FUNCION PARA VERIFICAR QUE SE CUMPLEN LAS EXPRESIONES
            Color bad = new Color();
            Color good = new Color();
            bad = Color.DarkOrange;
            good = Color.White;
            string[] auxAtributos = atributos.Split(',');
           
            for(int i=0; i<numeroInstancias-1; i++)
            {
                for(int j=1; j<=cantidadAtributos; j++)
                {
                    Regex rgx = new Regex(expRegular[auxAtributos[j - 1]].ToString());
                    //dataGrid.Rows[i].Cells[j].Value.ToString());
                    if (rgx.IsMatch(dataGrid.Rows[i].Cells[j].Value.ToString()))
                    {
                        dataGrid.Rows[i].Cells[j].Style.BackColor = good;
                    }
                    else
                    {
                        dataGrid.Rows[i].Cells[j].Style.BackColor = bad;
                    }
                }
            }
        }

        public void AnalisisNumerico(List<string> lista)
        {
            List<double> listaNumerica = new List<double>();
            List<double> elementosVarianza = new List<double>();
            double varianza = 0;
            double desvest = 0;
            lista.Sort();
            double media = 0; //promedio
            int medianaPos = 0; //Posicion del valor medio
            string mediana = ""; //Valor medio en forma de texto
            string moda = ""; //valor mas repetido en el conjunto de datos
            string mensaje = "";
            
            //calcular media / promedio
            for (int i=0; i < lista.Count; i++)
            {
                media = media + double.Parse(lista[i]);
                listaNumerica.Add(double.Parse(lista[i]));
            }
            media = media / lista.Count;
            
            //obtener mediana / valor a la mitad del conjunto
            medianaPos = lista.Count / 2;
            mediana = lista[medianaPos];

            //Obtener moda / valor mas repetido
            moda = ObtenerModa(lista);

            //Restar la media a cada elemento
            for(int i = 0; i<lista.Count; i++)
            {
                elementosVarianza.Add(double.Parse(lista[i]) - media);
            }
            //Elevar al cuadrado cada elemento
            for(int i = 0; i<elementosVarianza.Count; i++)
            {
                elementosVarianza[i] = elementosVarianza[i] * elementosVarianza[i];
            }
            //sumar cada elemento de la lista varianza
            for(int i = 0; i<elementosVarianza.Count; i++)
            {
                varianza = varianza + elementosVarianza[i];
            }
            varianza = varianza / (elementosVarianza.Count - 1);
            desvest = Math.Sqrt(varianza);
            //Resultados del analisis univariable numerico
            mensaje += "Cantidad de elementos: " + lista.Count + "\n";
            mensaje += "Min: " + lista[0] + "\n";
            mensaje += "Max: " + lista[lista.Count-1] + "\n";
            mensaje += "Media: " + media.ToString() + "\n";
            mensaje += "Mediana: " + mediana + "\n";
            mensaje += "Moda: " + moda + "\n";
            mensaje += "Desviacion estandar: " + desvest + "\n";
            MessageBox.Show(mensaje);
            BoxPlot boxPlot = new BoxPlot(listaNumerica);
            boxPlot.ShowDialog();
        }
        
        public void AnalisisCategorico(List<string> lista)
        {
            Dictionary<string, int> indice = new Dictionary<string, int>();
            string nombreAtributos = "";
            string aux = "";
            List<string> llaves = new List<string>();
            List<int> valores = new List<int>(); 
            for (int i=0; i<lista.Count; i++)
            {//Guarda en un diccionario cuantas veces se repite cada atributo
                if (indice.ContainsKey(lista[i]))
                {
                    indice[lista[i]] = indice[lista[i]] + 1;
                }
                else
                {
                    indice.Add(lista[i], 1);
                    nombreAtributos += lista[i] + ",";
                }
            }
            for(int i =0; i<nombreAtributos.Length-1; i++)
            {
                aux += nombreAtributos[i];
            }
            string[] separado = aux.Split(',');
            for(int i=0; i<separado.Count(); i++)
            {
                llaves.Add(separado[i]);
                valores.Add(indice[separado[i]]);
            }

            
            Grafico grafico = new Grafico(llaves, valores);
            grafico.ShowDialog();

        }
        public string ObtenerModa(List<string> lista)
        {
            string masRepetido = "";
            int vecesRepetido = 0;
            int vecesActual;
            for(int i=0; i<lista.Count; i++)
            {
                vecesActual = 0;
                for(int j = 0; j<lista.Count; j++)
                {
                    if (lista[i] == lista[j])
                    {
                        vecesActual++;
                        if (vecesActual > vecesRepetido)
                        {
                            masRepetido = lista[i];
                            vecesRepetido = vecesActual;
                        }
                        else if(vecesActual == vecesRepetido && lista[i] != lista[j])
                        {
                            masRepetido = masRepetido + " | " + lista[i];
                        }
                    }
                }
            }
            return masRepetido;
        }
        /*
        public void Guardar()
        {
            string aux;
            string archivo2 = nombreArchivo + "copy.txt";

            StreamReader lector = new StreamReader(nombreArchivo);
            StreamWriter escritor = new StreamWriter(archivo2);
            while (true) //Guardar todo hasta la seccion de los datos
            {
                aux = lector.ReadLine();
                escritor.WriteLine(aux);
                if (aux == "@data") break;
            }
            while (!lector.EndOfStream)
            {
                aux = lector.ReadLine();
                string[] valores = aux.Split(',');
                //cantAtributos = valores.Count();
                //atrib = 1;
                for(int i=0; i<valores.Count()-1; i++)
                {
                    escritor.Write(valores[i]);
                    escritor.Write(",");
                }
                if (!lector.EndOfStream)
                {
                    escritor.WriteLine(valores[cantidadAtributos - 1]);
                }
                else
                {
                    escritor.Write(valores[cantidadAtributos - 1]);
                }
            }
            escritor.Close();
            lector.Close();
            File.Delete(nombreArchivo);
            MessageBox.Show("Si pasa");
            File.Move(archivo2, nombreArchivo);
        }
        */
        public void GuardarCambios(DataGridView dataGrid)
        {
            string aux;
            string archivo2 = nombreArchivo + "copy.txt";
            StreamReader lector = new StreamReader(nombreArchivo);
            StreamWriter escritor = new StreamWriter(archivo2);
            while (true) //Guardar todo hasta la seccion de los datos
            {
                aux = lector.ReadLine();
                escritor.WriteLine(aux);
                if (aux == "@data") break;
            }
            for (int i = 0; i < numeroInstancias - 1; i++)
            {
                for (int j = 1; j <= cantidadAtributos; j++)
                {
                    escritor.Write(dataGrid.Rows[i].Cells[j].Value.ToString());
                    if(j <= cantidadAtributos-1) escritor.Write(',');
                }
                if (i < numeroInstancias-2) escritor.Write("\n");
            }
            escritor.Close();
            lector.Close();
            File.Delete(nombreArchivo);
            MessageBox.Show("Se ha guardado con exito");
            File.Move(archivo2, nombreArchivo);
        }

        public void GuardarComo(string nombreNuevo, DataGridView dataGrid)
        {
            string aux;
            string archivo2 = nombreNuevo;
            StreamReader lector = new StreamReader(nombreArchivo);
            StreamWriter escritor = new StreamWriter(archivo2);
            while (true) //Guardar todo hasta la seccion de los datos
            {
                aux = lector.ReadLine();
                escritor.WriteLine(aux);
                if (aux == "@data") break;
            }
            for (int i = 0; i < numeroInstancias - 1; i++)
            {
                for (int j = 1; j <= cantidadAtributos; j++)
                {
                    escritor.Write(dataGrid.Rows[i].Cells[j].Value.ToString());
                    if (j <= cantidadAtributos - 1) escritor.Write(',');
                }
                if (i < numeroInstancias - 2) escritor.Write("\n");
            }
            escritor.Close();
            lector.Close();
            MessageBox.Show("Se ha guardado con exito\n" + nombreNuevo);
        }
    }
}