var eventos = {
	xviii: {
		"_1700": [
			{
				titulo: "El rey Felipe sube al trono",
				descripcion: "El 16 de noviembre, el duque de Anjou, Felipe de Borbón, es proclamado rey, convirtiéndose en el primer rey de la Casa de Borbón del reino español."
			}
		],
		"_1752": [
			{
				titulo: "Nacimiento del obispo Cabañas",
				descripcion: "El 3 de mayo, nace en Navarra, Juan Cruz Ruiz de Cabañas y Crespo, futuro  obispo de la diócesis de Guadalajara de 1795 a 1824."
			}
		],
		"_1757": [
			{
				titulo: "Nacimiento de Manuel Tolsá",
				descripcion: "El 4 de mayo, nace en Enguera, Valencia, don Manuel Vicente Agustín Tolsá y Sarrión, reconocido arquitecto y escultor quién fue el iniciador del estilo neoclásico en la Nueva España."
			}
		],
		"_1767": [
			{
				titulo: "Herencia de fortuna",
				descripcion: "El acaudalado catalán José Llorens Comelles lega toda su fortuna para la  construcción de un hospital y una casa de asistencia de niños expósitos bajo la advocación de San José, cuya dirección debía quedar bajo el gobierno del obispado. Dicha sucesión ocasiona grandes conflictos legales entre los albaceas, la iglesia y el gobierno."
			},
			{
				titulo: "La expulsión de los jesuitas",
				descripcion: "La Compañía de Jesús es expulsada de los dominios del reino español. Carlos III ordena la expulsión acusando a la orden religiosa de haber participado en el motín de Esquilache de 1766, aunque en realidad mucho se debió a la influencia y poderío económico y social que poseían."
			}
		],
		"_1772": [
			{
				titulo: "Nacimiento de José Gutiérrez",
				descripcion: "Nace en Macharaviaya, municipio de Málaga, José Gutiérrez, arquitecto y académico de mérito de la Real Academia de San Carlos y primer maestro de arquitectura en la Nueva Galicia."
			}
		],
		"_1780": [
			{
				titulo: "Manuel Tolsá se traslada a Madrid",
				descripcion: "Manuel Tolsá se traslada a Madrid a estudiar en la Academia de San Fernando donde tiene como maestro al escultor Juan Pascual Mena que había sido maestro de José Puchol y José Arias; el segundo vendría a México para ser director de escultura en la Academia de San Carlos en la Nueva España."
			}
		],
		"_1781": [
			{
				titulo: "La Real Academia de San Carlos",
				descripcion: "El Rey Carlos III funda la Real Academia de San Carlos de las Nobles Artes de la Nueva España, la primera academia fundada en América."
			}
		],
		"_1786": [
			{
				titulo: "Sequía en la Nueva Galicia",
				descripcion: "Una gran sequía y desabasto de granos azota la capital del virreinato neogallego;  la hambruna toca las puertas de la ciudad y causa la muerte de muchos de sus pobladores."
			},
			{
				titulo: "Las intendencias en la Nueva España",
				descripcion: "Se crean las intendencias para que la Corona ejerza un control más rígido sobre la población y las regiones novohispanas."
			}
		],
		"_1787": [
			{
				titulo: "Hospital San Miguel de Belén",
				descripcion: "Ante la epidemia de cólera, Fray Antonio Alcalde inicia un proyecto para “airear” la ciudad, proyectando hacia el norte el Santuario de la Virgen de Guadalupe y la construcción del Hospital Real de San Miguel de Belén."
			}
		],
		"_1788": [
			{
				titulo: "Carlos III y José Arias",
				descripcion: "José Arias fallece repentinamente y deja vacante el puesto de director de escultura de la Academia de San Carlos en la Nueva España. El 14 de diciembre el rey Carlos III muere en España a los setenta y dos años de edad."
			}
		],
		"_1789": [
			{
				titulo: "La revolución francesa",
				descripcion: "El 14 de julio estalla la Revolución francesa con la toma de la Bastilla. El 26 de agosto se aprueba la Declaración de los derechos del hombre y del ciudadano en París."
			}
		],
		"_1791": [
			{
				titulo: "Manuel Tolsá llega a la  Nueva España",
				descripcion: "Arriba Manuel Tolsá a la ciudad de México para hacerse cargo de la dirección de escultura de la Real Academia de San Carlos."
			}
		],
		"_1792": [
			{
				titulo: "La Real Universidad de Guadalajara",
				descripcion: "El 7 de agosto, fallece el obispo Fray Antonio Alcalde y Barriga, quien dejó grandes beneficios a la ciudad de Guadalajara, como la construcción del Hospital San Miguel de Belén (hoy Antiguo Hospital Civil), el Santuario de Guadalupe, la Real Universidad de Guadalajara y un sinfín de obras de beneficencia para los más necesitados. El 3 de noviembre, se inaugura la Real Universidad de Guadalajara, la segunda universidad de la Nueva España. Adopta el modelo de la Universidad de Salamanca (en ese momento, referente europeo de educación universitaria) e inicia con las cátedras de medicina, derecho, teología y filosofía, con sede en el Antiguo Colegio de Santo Tomás que había quedado deshabilitado tras la expulsión de la orden de la Compañía de Jesús."
			},
			{
				titulo: "La primera imprenta en Guadalajara",
				descripcion: "Con el decreto del 7 de febrero de 1792, que le había otorgado la Real Audiencia de Guadalajara, Mariano Valdés, hijo de don Manuel Antonio Valdés, impresor en la Ciudad de México, establece la imprenta en Guadalajara."
			}
		],
		"_1796": [
			{
				titulo: "Cabañas arriba a la Nueva Galicia",
				descripcion: "Después de casi un año de su llegada a Veracruz, el obispo Juan Cruz Ruiz de Cabañas y Crespo arriba a Guadalajara para hacerse cargo de la diócesis. Desde su recibimiento, el obispo se da cuenta del hambre y la miseria que existen en un gran sector de la población. Solicita autorización a España para erigir un edificio para alojar a los desamparados, alimentar a los hambrientos y priorizar la enseñanza para quienes la necesitan. Mediante Cédula Real, Carlos IV aprueba que el obispo Cabañas funde una casa de expósitos y que elija el terreno para su construcción."
			},
			{
				titulo: "Se inaugura la efigie a Carlos IV",
				descripcion: "El 9 de diciembre, fecha del cumpleaños de la reina María Luisa, se inaugura la estatua provisional en madera del monarca Carlos IV,  mandada a hacer por el virrey Miguel de la Grúa Salamanca y Branciforte quien le encomienda este importante trabajo a Manuel Tolsá. Después de reunir los fondos para el vaciado de la escultura, en 1803, exactamente siete años más tarde, se devela el monumento ecuestre en bronce del monarca en la plaza principal en presencia del ahora virrey José de Iturrigaray. Hoy en día se conoce esta estatua coloquialmente con el nombre del Caballito."
			}
		],
	},
	xix: {
		"_1801": [
			{
				titulo: "La elección del terreno",
				descripcion: "Al oriente de Guadalajara, muy cerca del río de San Juan de Dios, existía una vieja casa con huerta y solar, propiedad del Santuario de Nuestra Señora de la Soledad, que fue vendida el 19 de mayo al licenciado Pedro Díaz Escandón por mil ochocientos ochenta pesos. Posteriormente, Escandón cede esta propiedad para la construcción de lo que sería poco tiempo después la Casa de la Misericordia."
			}
		],
		"_1804-1805": [
			{
				titulo: "La Casa de la Misericordia",
				descripcion: "Con el terreno ya disponible, el obispo Cabañas solicita un proyecto arquitectónico a la Real Academia de San Carlos para la construcción de la Casa de la Misericordia. El Director de la Academia, Don Antonio Jerónimo Gil, y Manuel Tolsá, entonces director de arquitectura de la Academia, designan entre sus egresados al más capacitado para hacerse cargo de la construcción del inmueble. El arquitecto José Gutiérrez, académico de mérito de la Real Academia, a partir de ese momento tendrá la dirección total de la obra, y además se encargará de impartir clases de arquitectura en Guadalajara. La construcción de la Casa de la Misericordia se inicia en febrero de 1805 bajo la dirección de José Gutiérrez quien ya se encontraba en Guadalajara. Paralelamente el arquitecto construye el Puente Verde (ubicado en la Calzada Independencia a la altura de Av. Revolución) para asegurar las vías de comunicación y de abastecimiento del centro de la ciudad a la casa de expósitos."
			}
		],
		"_1810": [
			{
				titulo: "Se convierte en cuartel",
				descripcion: "El 1° de febrero, con la construcción casi finalizada (a excepción de la cúpula), la Casa de la Misericordia recibe a los primeros asilados –66 en total, entre niños y desvalidos–. Este mismo año se suspenden las obras de construcción debido al movimiento insurgente, lo que provoca que la casa sea convertida en cuartel militar. Al mismo tiempo, el estallido de la guerra de Independencia hace que el arquitecto José Gutiérrez se aliste en las tropas de Félix María Calleja como artillero y posteriormente regrese a la ciudad de México donde le otorgan la dirección de arquitectura en la Academia de San Carlos."
			},
			{
				titulo: "La guerra de Independencia",
				descripcion: "El 16 de septiembre, Miguel Hidalgo y Costilla hace el llamado a las armas en el pueblo de Dolores, Guanajuato. Inicia la guerra de Independencia."
			},
			{
				titulo: "Miguel Hidalgo en Guadalajara",
				descripcion: "El 26 de noviembre, Miguel Hidalgo y Costilla hace su entrada triunfal en Guadalajara, después de haber pasado por los municipios de Zamora, La Barca, Atequiza y Tlaquepaque. El 20 de diciembre, se publica en Guadalajara El Despertador Americano, primer periódico publicado por los insurgentes durante la guerra de Independencia."
			},
			{
				titulo: "Manuel Gómez Ibarra",
				descripcion: "Nace en la hacienda 'La Labor', Manuel Gómez Ibarra, destacado arquitecto, discípulo de José Gutiérrez."
			}
		],
		"_1811": [
			{
				titulo: "La batalla del puente de Calderón",
				descripcion: "El 17 de enero se da la victoria del ejército realista sobre los insurgentes en la batalla del Puente de Calderón."
			}
		],
		"_1816": [
			{
				titulo: "Muere Manuel Tolsá",
				descripcion: "El 24 de diciembre, muere don Manuel Vicente Agustín Tolsá y Sarrión en la ciudad de México a causa de una úlcera gástrica."
			}
		],
		"_1821": [
			{
				titulo: "La Junta Patriótica de la Nueva Galicia",
				descripcion: "Tras el triunfo del Ejército Trigarante, el 22 de septiembre, se instala en Guadalajara la Junta Patriótica de la Nueva Galicia, destinada a promover la ilustración, las artes, la agricultura y la moral pública. En dicha junta el obispo Cabañas pronuncia un discurso en el que eleva la educación de la juventud y las artes al progreso social, así como la necesidad del fomento de la agricultura y del comercio como fuentes de riqueza pública."
			},
			{
				titulo: "Los Tratados de Córdoba",
				descripcion: "El 24 de agosto, Agustín de Iturbide y el virrey Juan O' Donojú firman los Tratados de Córdoba, con los cuales se reconoce la independencia de México y se asume como forma de gobierno una monarquía institucional moderada. El 27 de septiembre, se consuma la independencia. El Ejército Trigarante al mando de Iturbide desfila victorioso por las calles de la ciudad de México."
			}
		],
		"_1823": [
			{
				titulo: "El estado libre y soberano de Jalisco",
				descripcion: "El 16 de junio, el imperialismo y el colonialismo quedan atrás, y surge el Estado Libre y Soberano de Xalisco."
			}
		],
		"_1824": [
			{
				titulo: "La primera Constitución",
				descripcion: "Se promulga la primera Constitución Federal de los Estados Unidos Mexicanos."
			},
			{
				titulo: "Muere el obispo Cabañas",
				descripcion: "El 28 de noviembre, muere el obispo Juan Cruz Ruiz de Cabañas y Crespo en la ciudad de Guadalajara."
			}
		],
		"_1825": [
			{
				titulo: "José Gutiérrez en la ciudad",
				descripcion: "En enero, Prisciliano Sánchez Padilla toma posesión como primer gobernador del estado de Jalisco. José Gutiérrez regresa a Guadalajara para reanudar los trabajos en el templo del Sagrario y realizar las modificaciones en la portada del antiguo templo de Santo Tomás."
			}
		],
		"_1826": [
			{
				titulo: "El Instituto de Ciencias de Jalisco",
				descripcion: "Se instaura el Instituto de Ciencias de Jalisco, importante espacio académico para el cultivo de la arquitectura en Guadalajara. Debido a la inestabilidad gubernamental, el instituto cierra sus puertas en 1832."
			},
			{
				titulo: "El gobernador Prisciliano Sánchez",
				descripcion: "El 30 de diciembre muere Prisciliano Sánchez, el primer gobernador constitucional del estado de Jalisco."
			}
		],
		"_1828": [
			{
				titulo: "Se devuelve la Casa de la Misericordia",
				descripcion: "Se devuelven las instalaciones de la Casa de la Misericordia, bajo el gobierno de Juan Nepomuceno Cumplido, después de albergar al cuartel militar durante el gobierno insurgente. Se reanudan los trabajos indispensables para que el edificio vuelva a estar en óptimas condiciones."
			}
		],
		"_1833": [
			{
				titulo: "Cólera en Guadalajara",
				descripcion: "El martes, 3 de agosto, se propaga una epidemia de cólera morbus o cólera grande, como pasó a ser llamada por la tradición tapatía. Esta enfermedad fue tan mortífera que muchas de las actividades urbanas y familiares se desquiciaron; en el transcurso de ese día fallecieron 238 personas."
			}
		],
		"_1834": [
			{
				titulo: "Muere José Gutiérrez",
				descripcion: "Fallece el arquitecto José Gutierrez en Guadalajara a la edad de sesenta y tres años, víctima de irritación corporal, que ahora correspondería a una fuerte infección."
			}
		],
		"_1836": [
			{
				titulo: "La construcción de la cúpula",
				descripcion: "Bajo el mandato del obispo Diego de Aranda, Manuel Gómez Ibarra, discípulo del arquitecto José Gutiérrez, reinicia la construcción de la cúpula de la capilla de la Casa de la Misericordia."
			}
		],
		"_1842": [
			{
				titulo: "Antonio López de Santa Anna entra a Guadalajara",
				descripcion: "Antonio López de Santa Anna entra a la ciudad para imponer el centralismo."
			}
		],
		"_1850": [
			{
				titulo: "La Escuela de Artes y Oficios",
				descripcion: "Se funda  la Escuela de Artes y Oficios en Guadalajara."
			}
		],
		"_1852": [
			{
				titulo: "La cúpula es concluida",
				descripcion: "Manuel Gómez Ibarra concluye la construcción de la cúpula del edificio. La Casa de la Misericordia vuelve a ser utilizada como ciudadela, y los niños que la habitan son enviados a distintos lugares de la ciudad."
			}
		],
		"_1853": [
			{
				titulo: "Las Hermanas de la Caridad",
				descripcion: "Gracias a diversas gestiones, arriban a Guadalajara las Hermanas de la Caridad, quienes se encargarán de atender la institución a partir de ese momento."
			}
		],
		"_1857": [
			{
				titulo: "La construcción del Teatro Degollado",
				descripcion: "La Real Universidad de Guadalajara se clausura. El 5 de marzo, bajo el gobierno de José Santos Degollado, se coloca la primera piedra del Teatro Juan Ruiz de Alarcón, posteriormente conocido como el Teatro Degollado. La construcción estuvo a cargo del arquitecto Jacobo Gálvez."
			}
		],
		"_1859": [
			{
				titulo: "La Ley de Nacionalización de Bienes Eclesiásticos",
				descripcion: "El 12 de julio, en Veracruz, el presidente Benito Juárez expide la Ley de Nacionalización de los Bienes Eclesiásticos por la cual todas las propiedades de la iglesia pasan a ser dominio de la nación."
			},
			{
				titulo: "Sor Ignacia Osés",
				descripcion: "La Casa de la Misericordia recibe como directora a Sor Ignacia Osés quién desempeñará este cargo hasta 1874. Gracias a su impecable administración y generosidad, este periodo será conocido como la época de oro del hospicio.  Durante este tiempo, con el apoyo del gran benefactor Juan José Matute, se crean el asilo de mendigos, el departamento de cuna y talleres; se brinda ropa a los internos; y se decide bajo el gobierno eclesiástico que todos los niños expósitos llevarán el apellido Cabañas en memoria de su fundador."
			},
			{
				titulo: "La constitución Liberal",
				descripcion: "Se promulga la Constitución Política de la República Mexicana de ideología liberal durante la presidencia de Ignacio Comonfort."
			}
		],
		"_1862-1867": [
			{
				titulo: "La segunda intervención francesa",
				descripcion: "Debido a que el presidente Benito Juárez suspendió los pagos de la deuda externa del país en 1861, Francia, Inglaterra y España envían tropas a México. Con el Tratado de la Soledad, las tropas inglesas y españolas se retiran. El ejército francés continúa a Puebla donde es derrotado en una batalla el 5 de mayo. Posteriormente, los franceses reciben refuerzos y continúan hasta ocupar la ciudad de México y Guadalajara."
			}
		],
		"_1864": [
			{
				titulo: "Crisis en la Casa de la Misericordia",
				descripcion: "Como consecuencia de la Ley de Nacionalización de Bienes Eclesiásticos, la institución pasa por un periodo difícil debido a los recortes de gastos y el déficit. Fernando Díaz de Escandón, administrador de la Casa de la Misericordia, vende la huerta que se extiende al frente del edificio hasta el río San Juan de Dios para así poder sobrellevar la grave situación económica."
			},
			{
				titulo: "Maximiliano de Habsburgo",
				descripcion: "El archiduque Fernando Maximiliano de Habsburgo recibe el título de emperador de México, ofrecido por una comisión donde se encontraba Juan Nepomuceno Almonte, hijo de José María Morelos y Pavón, con el beneplácito de Napoleón III de Francia."
			}
		],
		"_1871": [
			{
				titulo: "El gobernador Ignacio Luis Vallarta Ogazón",
				descripcion: "Inicia el gobierno de Ignacio Luis Vallarta en Jalisco. Su gubernatura concluye en 1875.  Durante su gobierno, se fundó la Escuela de Agricultura y se estableció la obligatoriedad de la educación primaria.  Puerto Vallarta lleva tal nombre en su memoria."
			}
		],
		"_1874": [
			{
				titulo: "La expulsión de las Hermanas de la Caridad",
				descripcion: "A causa de las Leyes de Reforma, las Hermanas de la Caridad son expulsadas del país. El hospicio vuelve a tener dificultades económicas e higiénicas. El  relajamiento de las normas de salubridad provoca una epidemia en 1879 con un saldo de 207 infectados y 14 muertos, entre ellos la directora Luisa del Castillo."
			}
		],
		"_1876": [
			{
				titulo: "Primer mandato de Porfirio Díaz",
				descripcion: "Después de la Revolución de Tuxtepec, el 24 de noviembre, Porfirio Díaz Mori inicia su primer mandato que finaliza el 6 de diciembre del mismo año."
			}
		],
		"_1888": [
			{
				titulo: "El Hospicio",
				descripcion: "Tras la secularización de los bienes eclesiásticos, la Casa de la Misericordia pasa a depender de la dirección de Beneficencia Pública del Gobierno del Estado, y oficialmente recibe el nombre de Hospicio de Guadalajara."
			},
			{
				titulo: "Nace José Clemente Orozco",
				descripcion: "El 23 de noviembre, el muralista José Clemente Ángel Orozco Flores nace en Zapotlán el Grande, Jalisco."
			}
		],
		"_1889": [
			{
				titulo: "El primer ferrocarril",
				descripcion: "El primer ferrocarril llega a Guadalajara desde la Ciudad de México. Muere asesinado el general Ramón Corona, gobernador del Estado de Jalisco."
			}
		],
		"_1893": [
			{
				titulo: "Porfirio Díaz es presidente por cuarta ocasión",
				descripcion: "Por cuarta vez, el general Porfirio Díaz Mori rinde protesta como Presidente Constitucional."
			}
		],
		"_1899": [
			{
				titulo: "Epidemias de tifo en México",
				descripcion: "Se propagan epidemias de tifo y viruela en el país que ocasionan más de 50,000 muertos."
			}
		]
	},
	xx:{
		"_1902": [
			{
				titulo: "El periódico Regeneración",
				descripcion: "Los hermanos Ricardo y Jesús Flores Magón fundan el periódico Regeneración, por medio del cual atacan la dictadura de Porfirio Díaz. En 1902, Ricardo y Enrique Flores Magón editan El hijo de El Ahuizote. Ambos periódicos fueron suprimidos por el régimen dictatorial de Porfirio Díaz."
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
		"_19": [
			{
				titulo: "",
				descripcion: ""
			}
		],
	}
}