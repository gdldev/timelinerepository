import App from './App/index';

let app = new App("#app");

app.start();

export default app;