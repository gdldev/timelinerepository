import LineaCronologica from '../LineaCronologica/index'
//import EventoHistorico from './EventoHistorico/index'

class App {

    constructor(container) {
        this.container = document.querySelector(container);
        this.components = {
            LineaCronologica: new LineaCronologica(),
            //EventoHistorico
        }
        this.eventos = [];
    }

    getParams() {
        let prmstr = window.location.search.substr(1);
        return prmstr != null && prmstr != "" ? this.paramsToArray(prmstr) : {};
    }

    paramsToArray(prmstr) {
        let params = {};
        let prmarr = prmstr.split("&");
        for (let i = 0; i < prmarr.length; i++) {
            let tmparr = prmarr[i].split("=");
            params[tmparr[0]] = tmparr[1];
        }
        return params;
    }
    
    loadComponent(args) {
        if (Object.keys(args).length) {
            //return EventoHistorico.start(args);
        }
        return this.components.LineaCronologica.start({
            container: this.container,
            eventos: this.eventos
        });
    }

    start() {
        this.fetchEvents().then( eventos => {
            this.eventos = eventos;
            this.loadComponent(this.getParams());
        })
    }

    async fetchEvents(){
        return await fetch("./eventos.json").then(response => response.json());
    }
}

export default App;