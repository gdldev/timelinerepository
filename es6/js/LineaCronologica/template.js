class LineaCronologicaView {

    constructor() {
        this.container = document.createElement('div');
    }

    show(args) {
        this.eventos = args.eventos;
        let main = args.container;
        main.innerHTML = "";
        this.container.id="lineaCronologica";
        this.container.className = "columns fullscreen";
        this.container.innerHTML = this.getTemplate();
        main.appendChild(this.container);
    }
    
    getTemplate() {
        let template = ``;
        for (let i in this.eventos){
            template+=`
            <div class="siglo column has-text-centered">
             <h1>${i.toUpperCase()}</h1>
                <div class="anioEventos fullscreen scrollbar" id="${i}">
                    ${this.getYearsTemplate(i,this.eventos[i])}
                </div>
            </div>`;
        }
        return template;
    }

    getYearsTemplate(siglo,data) {
        let template = ``;
        for (let i in data){
            template+=`
            <div class="anioEvento">
                ${this.getEventTemplate(siglo,i,data[i])}
            </div>`;
        }
        return template;
    }

    getEventTemplate(siglo,anio,eventos) {
        let showAnio = anio;
        showAnio = showAnio.replace("_","");
        showAnio = showAnio.slice(0,4);
        return`
        <a class="anio" ondblclick="window.location.href='./?siglo=${siglo}&anio=${anio}'">
            <h2 class="is-size-4">${showAnio}</h2>
            <img src="./img/${anio}/A.jpg">
        </a>`;
    }

    checkImageExists(imageUrl, callBack) {
        var imageData = new Image();
        imageData.onload = function() {
            callBack(true);
        };
        imageData.onerror = function() {
            callBack(false);
        };
        imageData.src = imageUrl;
    }


}

export default LineaCronologicaView;