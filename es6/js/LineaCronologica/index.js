import LineaCronologicaView from './template';
import DragScroll from '../Util/dragScroll';

let view = new LineaCronologicaView();

class LineaCronologica{

    start(args){
        this.eventos = args.eventos;
        view.show(args);
        this.addDragEvents();
    }

    addDragEvents(){
        for (let i in this.eventos){
            new DragScroll(i)
        }
    }
}

export default LineaCronologica;