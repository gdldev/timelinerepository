export default class {
    constructor(id){
        this.drag = false;
        this.startx = 0;
        this.diffx = 0;
        this.starty = 0;
        this.diffy = 0;
        this.el = document.getElementById(id);
        this.addEvent('mousedown', this.el, this.onMouseDown);
        this.addEvent('mousemove', this.el, this.onMouseMove);
        this.addEvent('mouseup', this.el, this.onMouseUp);
    }    
    addEvent(name, el, func) {
        el.addEventListener(name, func, false);
    }
    onMouseDown(e) {
        if (!e) { e = window.event; }
        if (e.target && e.target.nodeName === 'IMG') {
            e.preventDefault();
        } else if (e.srcElement && e.srcElement.nodeName === 'IMG') {
            e.returnValue = false;
        }
        this.startx = e.clientX + this.scrollLeft;
        this.starty = e.clientY + this.scrollTop;
        this.diffx = 0;
        this.diffy = 0;
        this.drag = true;
    }
    onMouseMove(e) {
        if (this.drag === true) {
            if (!e) { e = window.event; }
            this.diffx = (this.startx - (e.clientX + this.scrollLeft));
            this.diffy = (this.starty - (e.clientY + this.scrollTop));
            this.scrollLeft += this.diffx;
            this.scrollTop += this.diffy;
        }
    }
    onMouseUp(e) {
        var vm = this;
        if (!e) { e = window.event; }
        this.drag = false;
        var start = 1,
            animate = function () {
                var step = Math.sin(start);
                if (step <= 0) {
                    window.cancelAnimationFrame(animate);
                } else {
                    vm.scrollLeft += vm.diffx * step;
                    vm.scrollTop += vm.diffy * step;
                    start -= 0.02;
                    window.requestAnimationFrame(animate);
                }
            };
        animate();
    }
}